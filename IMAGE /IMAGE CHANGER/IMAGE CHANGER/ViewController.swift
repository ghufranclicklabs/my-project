//
//  ViewController.swift
//  IMAGE CHANGER
//
//  Created by clicklabs92 on 15/01/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var inputField: UITextField!
    
    @IBOutlet weak var myImageView: UIImageView!
    
    
    @IBOutlet weak var labelField: UILabel!
    
    @IBAction func clickedGuessButtonAction(sender: AnyObject) {
        
        println("Guess button clicked")
       // var randomX = Int(arc4random()%6)
        //println("randomX = \(randomX)")
        var guess = inputField.text?.toInt();
        
        let image1 = UIImage(named: "images/1.jpeg")
        let image2 = UIImage(named: "images/2.jpeg")
        let image3 = UIImage(named: "images/3.jpeg")
        let image4 = UIImage(named: "images/4.jpeg")
        let image5 = UIImage(named: "images/5.jpeg")
        let image6 = UIImage(named: "images/6.jpeg")
        let image7 = UIImage(named: "images/7.jpeg")
        let image8 = UIImage(named: "images/8.jpeg")
        let image9 = UIImage(named: "images/9.jpeg")
        let image10 = UIImage(named: "images/10.jpeg")
       
        if((inputField.text) != nil){
            if(guess == 0){
              labelField.text = "BAD"
                myImageView.image = UIImage(named: "1.jpeg")// THIS IS NOT WORKING
                myImageView.image=image1      // THIS IS NOT WORKING TOO
                inputField.resignFirstResponder();// hides keyboard

                       }else if(guess == 1) {
                labelField.text = " GOOD"
                myImageView.image = UIImage(named: "1.jpeg")
                inputField.resignFirstResponder();//hides keyboard

            } else if(guess == 2) {
                labelField.text = " VERY GOOD"
                myImageView.image = UIImage(named: "2.jpeg")
                inputField.resignFirstResponder();//hides keyboard
            } else if(guess == 3){
            labelField.text = " FANTASTIC"
            myImageView.image = UIImage(named: "3.jpeg")
            inputField.resignFirstResponder();//hides keyboard
           } else if(guess == 4){
            labelField.text = " "
            myImageView.image = UIImage(named: "4.jpeg")
            inputField.resignFirstResponder();//hides keyboard
          } else if(guess == 5){
            labelField.text = " BEAUTIFUL"
            myImageView.image = UIImage(named: "5.jpeg")
            inputField.resignFirstResponder();//hides keyboard
        } else if(guess == 6){
            labelField.text = " THINK"
            myImageView.image = UIImage(named: "6.jpeg")
            inputField.resignFirstResponder();//hides keyboard
        } else if(guess == 7){
            labelField.text = " BETTER"
            myImageView.image = UIImage(named: "7.jpeg")
            inputField.resignFirstResponder();//hides keyboard
        } else if(guess == 8){
           labelField.text = " BEST"
            myImageView.image = UIImage(named: "8.jpeg")
            inputField.resignFirstResponder();//hides keyboard
        } else if(guess == 9){
           labelField.text = " WONDERFUL"
            myImageView.image = UIImage(named: "9.jpeg")
            inputField.resignFirstResponder();//hides keyboard
        } else if(guess == 10) {
            labelField.text = " AMAZING"
            myImageView.image = UIImage(named: "10.jpeg")
            inputField.resignFirstResponder();//hides keyboard
        } else{
            labelField.text = "ENTER VALUE WITHIN 1 TO 10";             myImageView.image = UIImage(named: "out of range.jpeg")
            inputField.resignFirstResponder();// hides keyboard
        }
    }
    }

    override func viewDidLoad() {
        
       super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
}
   override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

