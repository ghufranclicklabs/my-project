//
//  ViewController.swift
//  Stop Watch
//
//  Created by clicklabs92 on 16/01/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var TimerLabel: UILabel!
    var Counter = 0
    var Timer = NSTimer()

    @IBAction func ResetButton(sender: AnyObject) {
        Timer.invalidate()
        Counter = 0
        TimerLabel.text = String(Counter)
    }
    
    @IBAction func PlayButton(sender: AnyObject) {
   
         Timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("UpdateTimer"), userInfo: nil, repeats: true)
        // Timer = NSTimer.scheduledTimerWithTimeInterval( 1, target: self , selector:<#Selector#>("UpdateTimer"), userInfo: nil, repeats: true)
    }
    
    func UpdateTimer(){
    TimerLabel.text = String(Counter++)
    }
    
    
    @IBAction func PauseButton(sender: AnyObject) {
        Timer.invalidate()
    }
    
    
    override func viewDidLoad() {
        TimerLabel.text = String(Counter)
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

