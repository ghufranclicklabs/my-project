//
//  ViewController.swift
//  STUDENTINFO
//
//  Created by clicklabs92 on 14/01/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    // label1 field
    
    @IBOutlet weak var label1: UILabel!
    
    @IBOutlet weak var myImageView: UIImageView!
    
    
    //text field
    
    @IBOutlet weak var nametext: UITextField!
    @IBOutlet weak var studytext: UITextField!
    @IBOutlet weak var agetext: UITextField!
    @IBOutlet weak var mathstext: UITextField!
    @IBOutlet weak var phytext: UITextField!
    @IBOutlet weak var chemtext: UITextField!
    @IBOutlet weak var cstext: UITextField!
    
    
  
    
    
    //label field
    
    @IBOutlet weak var resultlabel: UIButton!
    @IBOutlet weak var resetlabel: UIButton!
    @IBOutlet weak var namelabel: UILabel!
    @IBOutlet weak var studylabel: UILabel!
    @IBOutlet weak var agelabel: UILabel!
    @IBOutlet weak var mathslabel: UILabel!
    @IBOutlet weak var chemlabel: UILabel!
    @IBOutlet weak var phylabel: UILabel!
    @IBOutlet weak var cslabel: UILabel!
    @IBOutlet weak var totallabel: UILabel!
    @IBOutlet weak var gradelabel: UILabel!
    @IBOutlet weak var avglabel: UILabel!
    
    
       //result field
    @IBAction func resultlabel(sender: AnyObject) {
        
        
        if ( nametext.text == "" || studytext.text == "" ||
        agetext.text == ""
            || mathstext.text == "" || chemtext.text == "" || phytext.text == "" || cstext.text == "" ) {
                label1.text = "invalid Output"
                     } else {
            
            namelabel.text = nametext.text
            studylabel.text = studytext.text
            agelabel.text = agetext.text
            mathslabel.text = mathstext.text
            chemlabel.text = chemtext.text
            phylabel.text = phytext.text
            cslabel.text = cstext.text
        
        
        //variable declaration
        
            var maths1 = mathstext.text.toInt()
            var chem1 = chemtext.text.toInt()
            var phy1 = phytext.text.toInt()
            var cs1 = cstext.text.toInt()
            
             // total value
                var totalMarks = maths1! + chem1! + phy1! + cs1!
            
            //covert total into string
            var total : String = String(totalMarks)
            totallabel.text = total
            
            // average calculate
            var avgmarks = totalMarks / 4
            var avg : String = String (avgmarks)
            avglabel.text = avg
            
            if (avgmarks <= 100 && avgmarks  >= 75){
                gradelabel.text = "A+"
                myImageView.image = UIImage(named:"a.jpeg")
                
            } else if (avgmarks <= 74 && avgmarks >= 60){
                gradelabel.text = "B+"
                myImageView.image = UIImage(named:"b.jpeg")
                
            } else if (avgmarks <= 59 && avgmarks >= 45){
                gradelabel.text = "C+"
                myImageView.image = UIImage(named:"c.jpeg")
            } else if (avgmarks <= 44 && avgmarks >= 33){
                gradelabel.text = "D+"
                myImageView.image = UIImage(named:"d.jpeg")
            } else if ( avgmarks <= 32 && avgmarks > 00){
                gradelabel.text = "FAIL"
                myImageView.image = UIImage(named:"e.png")
            } else {
                gradelabel.text = "INVALID"
                myImageView.image = UIImage(named:"f.jpeg")
        }
        }
    }

     //reset button
        @IBAction func resetButton(sender: AnyObject) {
        
        label1.text = "Enter Value Of Student"
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

