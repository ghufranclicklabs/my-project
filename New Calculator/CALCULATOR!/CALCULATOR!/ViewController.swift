//
//  ViewController.swift
//  CALCULATOR!
//
//  Created by clicklabs92 on 14/01/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
   var isTypingNumber = false
    var firstNumber = 0.0
    var secondNumber = 0.0
    var operation = ""
    var isDotPressed = false
    var operationContinued = false
    var result = 0.0
    
    @IBOutlet weak var calculatorDisplay:UILabel!
    
    @IBAction func plusButton(sender: AnyObject) {
        isDotPressed = false
        operation = "+"
        firstNumber = (calculatorDisplay.text! as NSString).doubleValue
        calculatorDisplay.text = ""

    }
    @IBAction func minusButton(sender: AnyObject) {
         isDotPressed = false
                operation = "-"
        firstNumber = (calculatorDisplay.text! as NSString).doubleValue
        calculatorDisplay.text = ""

    }
    
    @IBAction func multiButton(sender: AnyObject) {
         isDotPressed = false
        operation = "*"
        firstNumber = (calculatorDisplay.text! as NSString).doubleValue
        calculatorDisplay.text = ""

    }
    
    @IBAction func divideButton(sender: AnyObject) {
        isDotPressed = false
        operation = "/"
        firstNumber = (calculatorDisplay.text! as NSString).doubleValue
        calculatorDisplay.text = ""

    }
    
    
    
    
    @IBAction func dotButton(sender: AnyObject) {
        if isDotPressed == false{
            var number = "."
            
            if isTypingNumber {
                calculatorDisplay.text = calculatorDisplay.text! + number
            } else {
                calculatorDisplay.text = number
                isTypingNumber = true
            }
            isDotPressed = true
        }
    }
    
    @IBAction func dig0(sender: AnyObject) {
        var number = "0"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text! + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
        
    }
    
    @IBAction func dig1(sender: AnyObject) {
        var number = "1"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text! + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }

    }
    
    @IBAction func dig2(sender: AnyObject) {
        var number = "2"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text! + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }

    }
    
    
    @IBAction func dig3(sender: AnyObject) {
        var number = "3"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text! + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }

    }
    
    @IBAction func dig4(sender: AnyObject) {
        var number = "4"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text! + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }

    }
    
    
    @IBAction func dig5(sender: AnyObject) {
        var number = "5"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text! + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
}
    
    @IBAction func dig6(sender: AnyObject) {
        var number = "6"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text! + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }

    }
    
    @IBAction func dig7(sender: AnyObject) {
        var number = "7"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text! + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }

    }
    
    @IBAction func dig8(sender: AnyObject) {
        var number = "8"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text! + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }

    }
    
    

    @IBAction func dig9(sender: AnyObject) {
        var number = "9"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text! + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }

    }
    
    @IBAction func equalButton(sender: AnyObject) {
        isTypingNumber = false
        secondNumber = (calculatorDisplay.text! as NSString).doubleValue
        
        if operation == "+" {
            
            result = firstNumber + secondNumber
            
        } else if operation == "-" {
            
            result = firstNumber - secondNumber
            
        } else if operation == "*" {
            
            result = firstNumber * secondNumber
            
        } else if operation == "/" {
            
            if (secondNumber == 0) {
                
                calculatorDisplay.text = "invalid result"
                
            } else {
                
                
                result = (firstNumber / secondNumber)
                
            }
        }
        calculatorDisplay.text = "\(result)"
        
    }
    

    
    @IBAction func resetButton(sender: AnyObject) {
        
        //    var arr = Array (arrayLiteral: calculatorDisplay.text)
          //  arr.removeLast()
            
           //  var str1 = arr.removeLast()
           // calculatorDisplay.text = String (str1!)
            
            var arr = calculatorDisplay.text
            if arr != nil {
                self.calculatorDisplay.text = "0"
            }
          }

    

   override func viewDidLoad() {
    super.viewDidLoad()
   //  Do any additional setup after loading the view, typically from a nib.
}

   override func didReceiveMemoryWarning() {
       super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
}


}





